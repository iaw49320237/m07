<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\CompraRequest;
use Products;

class CompraController extends Controller
{

    public function main()
    {
        /*Recuerda el estado de la compra y redirige a la pantalla en la que el usuario estaba antes: resumen, envio o confirmar */
        if(session('status') == 'envio'){
            return redirect('/compra/envio');
        } else if (session('status')== 'confirm'){
            return redirect('/compra/confirmar');
        } else  {
            return redirect('/compra/resumen');

        
        }
       

       
        
    }
    /**
     * Method to show the resume of the products in the chart
     */
    public function resumen(Request $request)
    {
        //Dummy: hay que cambiar la info por la información guardada en el carrito (session)
       
        $request->session()->put('status', 'resumen');
        //Dummy: hay que cambiar la info por la información guardada en el carrito (session)
        $products = [
<<<<<<< HEAD
            session()->get('carrito')
        ];
        
        

=======
            session()->get('products')
        ]; /********* */
>>>>>>> 188d51832ba8372918b89f0bcaad7ade11a75b33
        return view('compra/resumen')
            ->with('products', $products);
    }

    /**
     * Method to show and process the shipping form (envio)
     */
    public function envio()
    {
        session()->put('status', 'envio');
        return view('compra/envio');
    }
    /**
     * Method to show and process the shipping form (envio)
     */
    public function verificarEnvio(RegisterRequest $request)
    {
        $request->flash();
        $formOK = false;

        
        if($request) {
            $formOK = true;
            $file = $request -> file('image');
            $destinationPath = 'img/users';
            $originalFile = $file->getClientOriginalName();
            $file->move($destinationPath,$originalFile);
            $formOK = true;
            $request->session()->put('name', $request->input('name'));
            $request->session()->put('email', $request->input('email'));
            $request->session()->put('direccion', $request->input('direccion'));
            $request->session()->put('location', $request->input('location'));
            $request->session()->put('password', $request->input('password'));


        } 

        
        /*Una vez verificado se guarda la información de envio en la session*/

        //si el formulario se ha rellenado correctamente se redirecciona a la pagina de confirmación
        if ($formOK) {
           redirect('/compra/confirmar'); 
        } 
        return view('compra/envio');
        
       
    }
    /**
     * Method to show the list of procuts and shipping info
     */
    public function confirmar()
    {
        session(['status' => 'confirm']);
        $products = [];
        //Dummy: hay que cambiar la info por la información guardada en session
        foreach (session()->get('carrito') as $param) {
            array_push($products, (object) $param);
        }
        
        $shipping =  session()->get('shipping');
        
        return view('compra/confirmar')->with('products', $products)->with('shipping', $shipping);
        
    }
    public function addProduct(CompraRequest $request) {
       
        $carrito = $request->session()->get('carrito', []);
        array_push($carrito, $request->input('product'));
        $request->session()->put('carrito',$carrito);
        //esta linea lo que hace es que cada vez que se añade un producto no se guarde la pagina de la sesion y al procesar una compra de nuevo se vaya a laprimera pagina
        $request->session()->forget('status');
    }

    public function stock(Request $request) {
        $product = Product::stock($request->input('quantity'), $request->input('id'));
        if (empty($product)) {return false;} else 
        {
            return $product->get();
        }
    }

    public function compraTerminada(Request $request) {
        // Vaciamos el carrito
        $request->session()->put('carrito', []);
        return view('compra/compraterminada');
    }
}

var express = require("express");
var path = require("path");
var router = express.Router();
var controllerDir = "/app/app/controllers";
var alumnosController = require(path.join(controllerDir, "alumnos"));
var docentesController = require(path.join(controllerDir, "docentes"));

router.get("/chat/:id", async (req, res, next) => {
    res.render("chat.pug");
});


router.get("/asignatura/new", async (req, res, next) => {

    const alumnos = await alumnosController.getAllAlumnos(req);
    const docentes = await docentesController.getAllDocentes(req);
    res.render("newAsignatura.pug", {alumnos: alumnos, docentes: docentes });
});





module.exports = router;
$(document).ready(function () {
  //Inicializa socket con IO
  const socket = io();
  const sala = window.location.href.split("/")[4];
  socket.emit("newRoom", sala);
  //Cuando cambia el select redirigimos a la URL del chat
  $('#selectRoom').on("change",()=>{
    var sala = $(this).find("option:selected").val();
    window.location.href = "/chat/"+sala;
  });
  
  //Accion cuando el usuario envia mensaje con submit
  $("#chat").submit(function (e) {
    e.preventDefault();
    var msg = $("#msg").val();
    var user = $("#autor").val();
    $("#chatBox").append(`<p><strong>${user}- </strong>${msg}<p>`);
    var msgToEmit = {
      user: user,
      msg: msg,
      room: sala
    }
    socket.emit("newMsg", msgToEmit);
  });

  //Acciones a realizar cuando se detecta actividad en el canal newMsg
  socket.on("newMsg", (data) => {
    $("#chatBox").append(`<p><strong>${data.user}- </strong>${data.msg}<p></p>`);
    
  });
});

require("dotenv").config();
var bodyParser = require("body-parser");
const port = process.env.SERVER_PORT || 6000;
var express = require("express"),
  app = express(),
  path = require("path"),
  mongoose = require("mongoose")
  
var io = require('socket.io')(server);
var server = require("http").createServer(app);

server.listen(port, (err, res) => {
  if (err) console.log(`ERROR: Connecting APP ${err}`);
  else console.log(`Server is running on port ${port}`);
});
mongoose.connect(
  `mongodb://devroot:devroot@mongo:27017/chat?authSource=admin`,
  { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true },
  (err, res) => {
    if (err) console.log(`ERROR: connecting to Database.  ${err}`);
    else console.log(`Database Online: ${process.env.MONGO_DB}`);
  }
);

// Import routes of our app

var routes = require("./app/routes/app");
var apiRouter = require("./app/routes/api");
var handlerError = require("./app/routes/handler");

// view engine setup and other configurations
app.set("views", path.join(__dirname,"app","views"));
app.set("view engine", "pug");
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

// Define routes using URL path

app.use("/", routes);
app.use(handlerError);
app.use("/api", apiRouter);

// body-parser config 

app.use(bodyParser.urlencoded({
   extended: true }));

/*Socket functions */

io.on('connection', (socket) => {
  console.log('Un cliente se ha conectado');
  socket.on('newMsg', (data) => {
      console.log(data);
      console.log(socket.rooms);
      if (data.room == "all") {
        socket.broadcast.emit("newMsg", data);
      } else {
        socket.to(data.room).emit("newMsg", data);
      }
  });
  socket.on('newRoom', (data) => {
      socket.leaveAll();
      socket.join(data);
      console.log(socket.rooms);
  })
});

module.exports = app;

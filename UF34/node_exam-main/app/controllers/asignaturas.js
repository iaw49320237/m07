var mongoose = require("mongoose"),
Asignatura = require("../models/asignatura");

exports.getAllAsignaturas = async () => {
    try {
        const asignaturas = await Asignatura.find();
        return asignaturas;
    } catch (error) {
        console.error(`Could not fetch Asignaturas ${error}`);
    }
};

exports.getAsignaturaById = async (req) => {
    try {
        var id = req.params.id;
        const asignatura = await Asignatura.findById(id);
        return asignatura;
    } catch (error) {
        console.error(`Could not fetch the specified Asignatura ${error}`);
    }
};

exports.deleteAsignaturaById = async (req) => {
    try {
        var id = req.params.id;
        const asignatura = await Asignatura.findByIdAndDelete(id);
        return asignatura;
    } catch (error) {
        console.error(` Could not delete Asignatura ${error}`);
    }
};

exports.updateAsignaturaById = async (req) => {
    try {
        var id = req.params.id;
        var dataToUpdate = {
            nombre: req.body.nombre,
             numHoras: req.body.numHoras,
             docente: req.body.docente,
             alumnos: req.body.alumnos
        }
        var response = await Asignatura.updateOne({ _id: id }, dataToUpdate);
        return response;
    } catch (error) {
        console.error(`Could not update "Asignatura"  ${error}`);
    }
};

exports.addAsignatura = async (req) => {
    var newAsignatura = {
      nombre: req.body.nombre,
      numHoras: req.body.numHoras,
      docente: req.body.docente,
      alumnos: req.body.alumnos,
    };
    try {
        var dataToSave = new Asignatura(newAsignatura);
        var response = await dataToSave.save();
        return response;
    } catch (error) {
        console.error(`Could not add the new Asignatura to database ${error}`);
    }
};

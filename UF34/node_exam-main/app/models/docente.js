var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DocenteSchema = new Schema({
    nombre: {type: String, required: true},
    apellido: {type: String, required: true},
});

module.exports = mongoose.model('Docente', DocenteSchema);
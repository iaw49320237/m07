<html>
  <head>
    <title>PHP Test</title>
  </head>
  <body>
    <?php
      $numeros = [-12, 84, 13, 20, -33, 101, 9];

      function separar($lista) {
        $array1 = [];
        $array2 = [];
        for ($i = 0; $i < count($lista); $i ++){
          if ($lista[$i] % 2 == 0) {
            array_push($array1, $lista[$i]);
          } else {
            array_push($array2, $lista[$i]);
          }
        }
        return [$array1, $array2];
      }

      print_r(separar($numeros));


    ?>
 
  </body>
</html>

var express = require("express");
var controllerDir = "/app/app/controllers";
var router = express.Router();
var path = require("path");
var alumnosController = require(path.join(controllerDir, "alumnos"));
var asignaturasController = require(path.join(controllerDir, "asignaturas"));
var docentesController = require(path.join(controllerDir, "docentes"));


// Rutas de las llamadas a los controladores de Alumnos, docentes y asignaturas

// Listar alumnos
router.get("/alumnos", async (req, res, next) => {
    var response = await alumnosController.getAllAlumnos();
    console.log(response);
    res.jsonp(response);
});

// Listar alumnos por id
router.get("/alumnos/:id", async (req, res, next) => {
    var response = await alumnosController.getAlumnoById(req);
    console.log(response);
    res.jsonp(response);
});

// Añadir nuevo alumno

router.post("/alumnos/new", async (req, res, next) => {
  var response = await alumnosController.addAlumno(req);
  console.log(response);
  res.jsonp(response);
});

// Editar alumno

router.put("/alumnos", async (req, res, next) => {
    var response = await alumnosController.updateAlumnoById(req);
    console.log(response);
    res.jsonp(response);
});

// Eliminar alumno 

router.delete("/alumnos/:id", async (req, res, next) => {
  var response = await alumnosController.deleteAlumnoById(req);
  console.log(response);
  res.jsonp(response);
});

// Listar docentes

router.get("/docentes", async (req, res, next) => {
    var response = await docentesController.getAllDocentes();
    console.log(response);
    res.jsonp(response);
});
 
// Listar docentes por id

router.get("/docentes/:id", async (req, res, next) => {
    var response = await docentesController.getDocenteById(req);
    console.log(response);
    res.jsonp(response);
});

// Añadir docente

router.post("/docentes/new", async (req, res, next) => {
  var response = await docentesController.addDocente(req);
  console.log(response);
  res.jsonp(response);
});

// Editar docente

router.put("/docentes", async (req, res, next) => {
    var response = await docentesController.updateDocenteById(req);
    console.log(response);
    res.jsonp(response);
});

// Eliminar docente

router.delete("/docentes/:id", async (req, res, next) => {
    var response = await docentesController.deleteDocenteById(req);
    console.log(response);
    res.jsonp(response);
});

// Listar Asignaturas

router.get("/asignaturas", async (req, res, next) => {
    var response = await asignaturasController.getAllAsignaturas();
    console.log(response);
    res.jsonp(response);
});

// Listar Asignaturas por id

router.get("/asignaturas/:id", async (req, res, next) => {
    var response = await asignaturasController.getAsignaturaById(req);
    console.log(response);
    res.jsonp(response);
});

// Añadir asignatura

router.post("/asignaturas/new", async (req, res, next) => {
  var response = await asignaturasController.addAsignatura(req);
  console.log(response);
  res.jsonp(response);
});

// Editar asignatura

router.put("/asignaturas", async (req, res, next) => {
    var response = await asignaturasController.updateAsignaturaById(req);
    console.log(response);
    res.jsonp(response);
});

// Eliminar Asignatura

router.delete("/asignaturas/:id", async (req, res, next) => {
    var response = await asignaturasController.deleteAsignaturaById(req);
    console.log(response);
    res.jsonp(response);
});

module.exports = router;
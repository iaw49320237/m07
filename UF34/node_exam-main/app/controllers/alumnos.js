var mongoose = require("mongoose"),
Alumno = require("../models/alumno");

exports.getAllAlumnos = async () => {
    try {
        const alumnos = await Alumno.find({});
        return alumnos;
    } catch (error) {
        console.error(` Could not fetch Alumnos ${error}`);
    }
};

exports.getAlumnoById = async (req) => {
    try {
        var id = req.params.id;
        const alumno = await Alumno.findById(id);
        return alumno;
    } catch (error) {
        console.error(`Could not get single Alumno ${error}`);
    }
};


exports.addAlumno = async (req) => {
  var newAlumno = {
    nombre: req.body.nombre,
    apellido: req.body.apellido,
  };
  try {
    var dataToSave = new Alumno(newAlumno);
    var response = await dataToSave.save();
    return response;
  } catch (error) {
    console.error(`Could not save Alumno to db ${error}`);
  }
};

exports.deleteAlumnoById = async (req) => {
    try {
        var id = req.params.id;
        const alumno = await Alumno.findByIdAndDelete(id);
        return alumno;
    } catch (error) {
        console.error(`Could not remove alumno  ${error}`);
    }
};

exports.updateAlumnoById = async (req) => {
    try {
        var id = req.params.id;
        var dataToUpdate = {
            nombre: req.body.nombre,
            apellido: req.body.apellido
        }
        var response = await Alumno.updateOne({_id: id}, dataToUpdate);
        return response;
    } catch (error) {
        console.error(`Could not update Alumno ${error}`);
    }
};


var mongoose = require("mongoose"),
  Docente = require("../models/docentes");

exports.getAllDocentes = async () => {
  try {
    const docentes = await Docente.find({});
    return docentes;
  } catch (error) {
    console.error(` Could not fetch Docentes ${error}`);
  }
};

exports.getDocenteById = async (req) => {
  try {
    var id = req.params.id;
    const docente = await Docente.findById(id);
    return docente;
  } catch (error) {
    console.error(`Could not get single Docente ${error}`);
  }
};

exports.addDocente = async (req) => {
  var newDocente = {
    nombre: req.body.nombre,
    apellido: req.body.apellido,
  };
  try {
    var dataToSave = new Docente(newDocente);
    var response = await dataToSave.save();
    return response;
  } catch (error) {
    console.error(`Could not save Docente to db ${error}`);
  }
};

exports.deleteDocenteById = async (req) => {
  try {
    var id = req.params.id;
    const docente = await Docente.findByIdAndDelete(id);
    return docente;
  } catch (error) {
    console.error(`Could not remove docente  ${error}`);
  }
};

exports.updateDocenteById = async (req) => {
  try {
    var id = req.params.id;
    var dataToUpdate = {
      nombre: req.body.nombre,
      apellido: req.body.apellido,
    };
    var response = await Docente.updateOne({ _id: id }, dataToUpdate);
    return response;
  } catch (error) {
    console.error(`Could not update Docente ${error}`);
  }
};

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AsignaturaSchema = new Schema({
    nombre: {type: String, required: true},
    numHoras: {type: String, required: true},
    docente: {type: String, required: true},
    alumnos: {type: [String]}
});

module.exports = mongoose.model('Asignatura', AsignaturaSchema);